import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminserviceService {
  private baseUrl = 'http://localhost:8080/api/admin';
 
  constructor(private http:HttpClient) { }
  private dUrl = 'http://localhost:8080/api/admins';
  
  adminRegistration(admin:any):Observable<any>{
    return this.http.post(this.baseUrl,admin);
  }
  adminLogin(admin:any):Observable<any>{
    return this.http.post(this.baseUrl,admin);
  }
}