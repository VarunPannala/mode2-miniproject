import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Donor } from '../donor';
import { DonorserviceService } from '../donorservice.service';

@Component({
  selector: 'app-donorapproval',
  templateUrl: './donorapproval.component.html',
  styleUrls: ['./donorapproval.component.css']
})
export class DonorapprovalComponent implements OnInit {

  donors: Observable<Donor[]>;
  constructor(private donorService:DonorserviceService) { }

  ngOnInit(): void {
    this.reloadData();
  }
  reloadData(){
    this.donors = this.donorService.getDonorsForApproval();
  }
}
