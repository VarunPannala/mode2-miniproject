import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MatToolbarModule} from '@angular/material/toolbar';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import {MatButtonModule} from '@angular/material/button';
import { DonorComponent } from './donor/donor.component';
import { RequestorComponent } from './requestor/requestor.component';
import { FormsModule } from '@angular/forms';
import { DonorregistrationComponent } from './donorregistration/donorregistration.component';
import { RequesterregistrationComponent } from './requesterregistration/requesterregistration.component';
import { AdminComponent } from './admin/admin.component';
import { RequesterentryComponent } from './requesterentry/requesterentry.component';
import { AdminaddComponent } from './adminadd/adminadd.component';
import { HomeComponent } from './home/home.component';
import { DonorentrryComponent } from './donorentrry/donorentrry.component';
import { CheckavailComponent } from './checkavail/checkavail.component';
import { AdminoptionsComponent } from './adminoptions/adminoptions.component';
import { DonorapprovalComponent } from './donorapproval/donorapproval.component';
import { RequesterapprovalComponent } from './requesterapproval/requesterapproval.component';
import { DonardetailsComponent } from './donardetails/donardetails.component';
import { RequesterdetailsComponent } from './requesterdetails/requesterdetails.component';

@NgModule({
  declarations: [ 
    AppComponent,
    DonorComponent,  
    RequestorComponent,
    DonorregistrationComponent,
    RequesterregistrationComponent,
    AdminComponent,
    RequesterentryComponent,
    AdminaddComponent,
    HomeComponent,
    DonorentrryComponent,
    CheckavailComponent,  
    AdminoptionsComponent, 
    DonorapprovalComponent,
    RequesterapprovalComponent,
    DonardetailsComponent,
    RequesterdetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatToolbarModule,
    MatButtonModule,
    HttpClientModule,
    FormsModule, 

    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {

 }
