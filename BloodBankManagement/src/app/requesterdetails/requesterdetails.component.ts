import { Component, Input, OnInit } from '@angular/core';
import { RequesterapprovalComponent } from '../requesterapproval/requesterapproval.component';
import { RequesterserviceService } from '../requesterservice.service';
import { Requester } from '../requestor';

@Component({
  selector: 'app-requesterdetails',
  templateUrl: './requesterdetails.component.html',
  styleUrls: ['./requesterdetails.component.css']
})
export class RequesterdetailsComponent implements OnInit {

  @Input()
  requester: Requester;

  acceptRequester() {
    this.requester.status = "accepted";
    this.requesterService.updateRequesterStatus(this.requester.requesterId,this.requester.status)
    .subscribe(
      data => {
        console.log(data);
        this.listrequester.reloadData();
      },
      error => console.log(error));
  }

  rejectRequester() {
    this.requester.status = "rejected";
    this.requesterService.updateRequesterStatus(this.requester.requesterId,this.requester.status)
    .subscribe(
      data => {
        console.log(data);
        this.listrequester.reloadData();
      },
      error => console.log(error));
  }

  constructor(private requesterService:RequesterserviceService, private listrequester:RequesterapprovalComponent) { }

  ngOnInit(): void {
  }
}
