import { Component, OnInit } from '@angular/core';
import { Donor } from '../donor';
import { DonorserviceService } from '../donorservice.service';

@Component({
  selector: 'app-donorentrry',
  templateUrl: './donorentrry.component.html',
  styleUrls: ['./donorentrry.component.css']
})
export class DonorentrryComponent implements OnInit {

  donorId:number;
  donor:Donor;
  errorMessage:boolean = true;
  contentDisplay:boolean = true;

  constructor(private donorService:DonorserviceService) { }

  


  ngOnInit(): void {
    this.donorId ;
  }
  
  private getDonorStatus() {
    this.donorService.getDonorById(this.donorId)
      .subscribe(donor => this.donor = donor);
  }

  onSubmit() {
    this.getDonorStatus();
  }
}
