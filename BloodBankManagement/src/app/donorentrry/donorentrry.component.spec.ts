import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DonorentrryComponent } from './donorentrry.component';

describe('DonorentrryComponent', () => {
  let component: DonorentrryComponent;
  let fixture: ComponentFixture<DonorentrryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DonorentrryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DonorentrryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
