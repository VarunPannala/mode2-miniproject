export class Donor{
    donorId:number;
    firstName:string;
    lastName:string;
    city:string;
    timeOfTheDay:Date;
    bloodGroup:string;
    bloodGlucoseLevel:number;
    notes:string;
    status:string;
}