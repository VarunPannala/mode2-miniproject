import { Component, OnInit } from '@angular/core';
import { Donor } from '../donor';
import { RequesterserviceService } from '../requesterservice.service';
import { Requester } from '../requestor';

@Component({
  selector: 'app-requestor',
  templateUrl: './requestor.component.html',
  styleUrls: ['./requestor.component.css']
})
export class RequestorComponent implements OnInit {
  requesterId:number;
  bloodGroup:string;
  donors: Donor[];
  requester:Requester;

  constructor(private requesterService:RequesterserviceService) { }

  ngOnInit(): void {
    this.requesterId = 0;
    this.bloodGroup = "bloodgroup";
  }
  
  private getRequesterStatus() {
    this.requesterService.getRequesterById(this.requesterId)
      .subscribe(requester => this.requester = requester);
  }

  private getDonors(){
    this.donors = [];
    this.requesterService.getDonorsByBloodGroup(this.bloodGroup)
      .subscribe(donors => this.donors = donors);
  }

  onSubmit() {
    this.getRequesterStatus();
  }

  onSubmitBloodGroup(){
    this.getDonors();
  }
}

