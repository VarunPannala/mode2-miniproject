import { Component, OnInit } from '@angular/core';
import { Donor } from '../donor';
import { RequesterserviceService } from '../requesterservice.service';
import { Requester } from '../requestor';

@Component({
  selector: 'app-checkavail',
  templateUrl: './checkavail.component.html',
  styleUrls: ['./checkavail.component.css']
})
export class CheckavailComponent implements OnInit {

  requesterId:number;
  bloodGroup:string;
  donors: Donor[];
  requester:Requester;

  constructor(private requesterService:RequesterserviceService) { }

  ngOnInit(): void {
    this.requesterId = 0;
    this.bloodGroup = "O+ve";
  }
  
  private getRequesterStatus() {
    this.requesterService.getRequesterById(this.requesterId)
      .subscribe(requester => this.requester = requester);
  }

  private getDonors(){
    this.donors = [];
    this.requesterService.getDonorsByBloodGroup(this.bloodGroup)
      .subscribe(donors => this.donors = donors);
  }

  onSubmit() {
    this.getRequesterStatus();
  }

  onSubmitBloodGroup(){
    this.getDonors();
  }
}

