import { Component, OnInit } from '@angular/core';
import { Admin } from '../admin';
import { AdminserviceService } from '../adminservice.service';

@Component({
  selector: 'app-adminadd',
  templateUrl: './adminadd.component.html',
  styleUrls: ['./adminadd.component.css']
})
export class AdminaddComponent implements OnInit {

  admin:Admin = new Admin();
  submitted:boolean = false;

  ngOnInit(): void {
  }
  constructor(private adminService:AdminserviceService) { }
  save(){
    this.adminService.adminRegistration(this.admin)
    .subscribe(
      data => {
        console.log(data);
        this.submitted = true;
      },
      error => console.log(error)
    );
    this.admin = new Admin();
  }
  onSubmit(){
    this.save();
  }
}
