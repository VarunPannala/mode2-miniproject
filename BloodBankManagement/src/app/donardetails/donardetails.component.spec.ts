import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DonardetailsComponent } from './donardetails.component';

describe('DonardetailsComponent', () => {
  let component: DonardetailsComponent;
  let fixture: ComponentFixture<DonardetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DonardetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DonardetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
