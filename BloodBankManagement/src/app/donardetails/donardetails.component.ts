import { Component, Input, OnInit } from '@angular/core';
import { Donor } from '../donor';
import { DonorapprovalComponent } from '../donorapproval/donorapproval.component';
import { DonorserviceService } from '../donorservice.service';

@Component({
  selector: 'app-donardetails',
  templateUrl: './donardetails.component.html',
  styleUrls: ['./donardetails.component.css']
})
export class DonardetailsComponent implements OnInit {

  @Input()
  donor: Donor;
 
  acceptDonor() {
    this.donor.status = "accepted";
    this.donorService.updateDonorStatus(this.donor.donorId,this.donor.status)
    .subscribe(
      data => {
        console.log(data);
        this.listdonor.reloadData();
      },
      error => console.log(error));
  }

  rejectDonor() {
    this.donor.status = "rejected";
    this.donorService.updateDonorStatus(this.donor.donorId,this.donor.status)
    .subscribe(
      data => {
        console.log(data);
        this.listdonor.reloadData();
      },
      error => console.log(error));
  }

  constructor(private donorService:DonorserviceService, private listdonor:DonorapprovalComponent) { }

  ngOnInit(): void {
  }
}
