import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequesterentryComponent } from './requesterentry.component';

describe('RequesterentryComponent', () => {
  let component: RequesterentryComponent;
  let fixture: ComponentFixture<RequesterentryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequesterentryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequesterentryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
