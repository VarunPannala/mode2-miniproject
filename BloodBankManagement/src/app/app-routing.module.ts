import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { AdminaddComponent } from './adminadd/adminadd.component';
import { AdminoptionsComponent } from './adminoptions/adminoptions.component';
import { CheckavailComponent } from './checkavail/checkavail.component';
import { DonorComponent } from './donor/donor.component';
import { DonorapprovalComponent } from './donorapproval/donorapproval.component';
import { DonorentrryComponent } from './donorentrry/donorentrry.component';
import { DonorregistrationComponent } from './donorregistration/donorregistration.component';
import { HomeComponent } from './home/home.component';
import { RequesterapprovalComponent } from './requesterapproval/requesterapproval.component';
import { RequesterentryComponent } from './requesterentry/requesterentry.component';
import { RequesterregistrationComponent } from './requesterregistration/requesterregistration.component';
import { RequestorComponent } from './requestor/requestor.component';

const routes: Routes = [
  {path:"donor", component:DonorComponent},
  {path:"dreg",component:DonorregistrationComponent},
  {path:"requester", component:RequestorComponent},
  {path:"rreg",component:RequesterregistrationComponent},
  {path:"dstatus",component:DonorentrryComponent},
  {path:"adminadd",component:AdminaddComponent},
  {path:"admin", component:AdminComponent},
  {path:"home",component:HomeComponent},
  {path:"rcheck",component:CheckavailComponent},
  {path:"adminLogin",component:AdminoptionsComponent},
  {path:"donorApproval",component:DonorapprovalComponent},
  {path:"requesterApproval",component:RequesterapprovalComponent},

  {path:"rstatus",component:RequesterentryComponent}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
