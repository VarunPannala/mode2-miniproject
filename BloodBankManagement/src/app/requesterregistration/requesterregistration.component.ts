import { Component, OnInit } from '@angular/core';
import { RequesterserviceService } from '../requesterservice.service';
import { Requester } from '../requestor';

@Component({
  selector: 'app-requesterregistration',
  templateUrl: './requesterregistration.component.html',
  styleUrls: ['./requesterregistration.component.css']
})
export class RequesterregistrationComponent implements OnInit {
  requester:Requester = new Requester();
  submitted:boolean = false;
  id:number
  ngOnInit(): void {
  } 

  constructor(private requesterService:RequesterserviceService) { }
  save(){
    this.requesterService.requesterRegistration(this.requester)
    .subscribe(
      data => {
        console.log(data);
        this.id = data.requesterId;
        this.submitted = true;
      },
      error => console.log(error)
    );
    this.requester = new Requester();
  }
  onSubmit(){
    this.save();
  }
}
