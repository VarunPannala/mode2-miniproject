import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DonorserviceService {
  private baseUrl = 'http://localhost:8080/api/donor';

  constructor(private http:HttpClient) { }

  donorRegistration(donor:any):Observable<any>{
    return this.http.post(this.baseUrl,donor);
  }

  private dUrl = 'http://localhost:8080/api/donors';
  getDonorById(donorId:number):Observable<any>{
    return this.http.get(`${this.dUrl}/${donorId}`);
  }
  getDonorsForApproval():Observable<any>{
    return this.http.get(this.dUrl);
  }

  updateDonorStatus(donorId:number,status:string):Observable<any>{
    return this.http.get(`${this.dUrl}/${donorId}/${status}`);
  }
}
