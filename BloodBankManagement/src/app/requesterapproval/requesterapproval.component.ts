import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { RequesterserviceService } from '../requesterservice.service';
import { Requester } from '../requestor';

@Component({
  selector: 'app-requesterapproval',
  templateUrl: './requesterapproval.component.html',
  styleUrls: ['./requesterapproval.component.css']
})
export class RequesterapprovalComponent implements OnInit {

  requesters: Observable<Requester[]>;
  constructor(private requesterService:RequesterserviceService) { }

  ngOnInit(): void {
    this.reloadData();
  }
  reloadData(){
    this.requesters = this.requesterService.getRequestersForApproval();
  }
}
