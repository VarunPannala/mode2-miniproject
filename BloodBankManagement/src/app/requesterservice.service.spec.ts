import { TestBed } from '@angular/core/testing';

import { RequesterserviceService } from './requesterservice.service';

describe('RequesterserviceService', () => {
  let service: RequesterserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RequesterserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
