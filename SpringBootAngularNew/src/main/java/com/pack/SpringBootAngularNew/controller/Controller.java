package com.pack.SpringBootAngularNew.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

 

import com.pack.SpringBootAngularNew.dao.*;
import com.pack.SpringBootAngularNew.model.*;
import com.pack.SpringBootAngularNew.service.AdminService;
import com.pack.SpringBootAngularNew.service.DonorService;
import com.pack.SpringBootAngularNew.service.RequesterService;
import com.pack.SpringBootAngularNew.service.RequesterService.*;

 

@CrossOrigin(origins="http://localhost:4200")
@RestController
@RequestMapping("/api")
public class Controller {
    private static final Logger LOGGER = LoggerFactory.getLogger(Controller.class);
	
	@Autowired(required=true)
	DonorService donorService; 
	
	@Autowired
	RequesterService requesterService; 
	
	
	@Autowired
	AdminService adminService;
	
	@PostMapping(value = "/donor")
	public ResponseEntity<Donor> postDonor(@RequestBody Donor donor) {
		try {
			LOGGER.info("Donor Created");
			return new ResponseEntity<>(donorService.donorRegistration(donor), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}
	}
	@PostMapping(value = "/admin")
	public ResponseEntity<Admin> adminAutentication(@RequestBody Admin admin) {
		try {
			LOGGER.info("Admin Autentication");

			return new ResponseEntity<>(adminService.adminAuthentication(admin), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}
	}
	

	@PostMapping(value = "/requester")
	public ResponseEntity<Requester> postRequester(@RequestBody Requester requester) {
		try {
			LOGGER.info("Donor Created");
			return new ResponseEntity<>(requesterService.requesterRegistration(requester), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	
	
	@GetMapping(value = "donors/bloodGroup/{bloodGroup}")
	  public ResponseEntity<List<Donor>> findDonarByBloodGroup(@PathVariable String bloodGroup) {
	    try {
			LOGGER.info("Search Donor By BloodGroup ");
	      List<Donor> donors = donorService.findDonarByBloodGroup(bloodGroup);

	      if (donors.isEmpty()) {
	        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	      }
	      return new ResponseEntity<>(donors, HttpStatus.OK);
	    } catch (Exception e) {
	      return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
	    }
	  }
	
	@GetMapping("/donors/{id}")
	public ResponseEntity<Donor> getDonorsById(@PathVariable("id") long id) {
		try {
			LOGGER.info("Search Donor By Id");
			return new ResponseEntity<>(donorService.donorStatus(id), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	@GetMapping("/requesters/{id}")
	public ResponseEntity<Requester> getRequestersById(@PathVariable("id") long id) {
		try {			
			LOGGER.info("Search Requester By Id");
			return new ResponseEntity<>(requesterService.requesterStatus(id), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}
	}
	@GetMapping("/requesters")
	public ResponseEntity<List<Requester>> getRequestersForApproval() {
		try {
			return new ResponseEntity<>(requesterService.getRequestersForApproval(), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	@GetMapping("/requesters/{requesterId}/{status}")
	public void requesterStatusUpdate(@PathVariable("requesterId") long requesterId,@PathVariable("status") String status) {
		try {
			requesterService.requesterStatusUpdate(requesterId, status);
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	@GetMapping("/donors")
	public ResponseEntity<List<Donor>> getDonorsForApproval() {
		try {
			return new ResponseEntity<>(donorService.getDonorsForApproval(), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	@GetMapping("/donors/{donorId}/{status}")
	public void donorStatusUpdate(@PathVariable("donorId") long donorId,@PathVariable("status") String status) {
		try {
			donorService.donorStatusUpdate(donorId, status);
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	

	
}


