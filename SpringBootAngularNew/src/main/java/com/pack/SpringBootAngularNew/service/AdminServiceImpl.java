package com.pack.SpringBootAngularNew.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pack.SpringBootAngularNew.dao.AdminRepository;
import com.pack.SpringBootAngularNew.dao.BloodBankRepository;
import com.pack.SpringBootAngularNew.model.Admin;
import com.pack.SpringBootAngularNew.model.Donor;

import java.util.Optional;


@Service
public class AdminServiceImpl implements AdminService{
	@Autowired
	AdminRepository adminRepository; 
	
	@Override
	public Admin adminRegistration(Admin admin) {
		return adminRepository
		        .save(new Admin(admin.getUserName(),admin.getPassword()));
	}	


	@Override
	public Admin adminAuthentication(Admin admin) {
		Optional<Admin> optional= adminRepository.findById(admin.getUserName());
		Admin databaseAdmin= null;
		if(optional.isPresent()) {
			databaseAdmin = optional.get();
			if(databaseAdmin.getPassword().equals(admin.getPassword())) {
				return admin;
			}
		}
		return null;
	}
	
	

		

}
