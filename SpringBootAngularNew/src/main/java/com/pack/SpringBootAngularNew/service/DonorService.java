package com.pack.SpringBootAngularNew.service;

import java.util.List;

import com.pack.SpringBootAngularNew.model.*;

public interface DonorService {
	public Donor donorRegistration(Donor donor);
	public List<Donor> findDonarByBloodGroup(String bloodGroup);
	public Donor donorStatus(long id);
	public List<Donor> getDonorsForApproval();
	public void donorStatusUpdate(Long donorId,String status);
}
