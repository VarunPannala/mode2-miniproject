package com.pack.SpringBootAngularNew.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pack.SpringBootAngularNew.controller.*;
import com.pack.SpringBootAngularNew.dao.BloodBankRepository;
import com.pack.SpringBootAngularNew.model.*;

@Service
public class DonorServiceImpl implements DonorService{
	
	@Autowired
	BloodBankRepository donorRepository; 

	@Override
	public Donor donorRegistration(Donor donor) {
		return donorRepository
		.save(new Donor(donor.getFirstName(),donor.getLastName(),donor.getCity(),donor.getBloodGroup(),donor.getTimeOfTheDay(),donor.getBloodGlucoseLevel(),donor.getNotes()));
	}

	@Override
	public List<Donor> findDonarByBloodGroup(String bloodGroup) {
		return donorRepository.findByBloodGroup(bloodGroup);
	}

	
	@Override
	public List<Donor> getDonorsForApproval() {
		return donorRepository.findAllDonorsForApproval();
	}

	@Override
	public void donorStatusUpdate(Long donorId, String status) {
		donorRepository.updateDonorStatus(status, donorId);
	}

	@Override
	public Donor donorStatus(long id) {
		Optional<Donor> optional= donorRepository.findById(id);
		Donor databaseDonor= null;
		if(optional.isPresent()) {
			databaseDonor = optional.get();
			return databaseDonor;
		}
		return null;
	}
	
	

}
