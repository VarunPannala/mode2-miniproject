package com.pack.SpringBootAngularNew.service;

import java.util.List;

import com.pack.SpringBootAngularNew.model.Requester;

public interface RequesterService {
	Requester requesterRegistration(Requester requester);
	Requester requesterStatus(long id);
	public List<Requester> getRequestersForApproval();
	public void requesterStatusUpdate(Long requesterId,String status);
	
}
