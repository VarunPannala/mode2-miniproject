package com.pack.SpringBootAngularNew.service;

import com.pack.SpringBootAngularNew.model.*;
public interface AdminService {
	public Admin adminAuthentication(Admin admin);
	public Admin adminRegistration(Admin Admin);

}

