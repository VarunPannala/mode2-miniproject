package com.pack.SpringBootAngularNew;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootAngularNewApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootAngularNewApplication.class, args);
		
	}

}
