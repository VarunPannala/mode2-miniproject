package com.pack.SpringBootAngularNew.dao;
import org.springframework.data.repository.CrudRepository;

import com.pack.SpringBootAngularNew.dao.*;
import com.pack.SpringBootAngularNew.model.*;
import com.pack.SpringBootAngularNew.service.DonorService;

public interface AdminRepository extends CrudRepository<Admin, String>{

}
