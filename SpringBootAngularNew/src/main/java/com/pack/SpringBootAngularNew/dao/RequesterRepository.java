package com.pack.SpringBootAngularNew.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.pack.SpringBootAngularNew.dao.*;
import com.pack.SpringBootAngularNew.model.*;
import com.pack.SpringBootAngularNew.service.DonorService;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface RequesterRepository extends CrudRepository<Requester, Long>{
	@Query(value="select * from requester where status = 'pending'", nativeQuery=true)
	public List<Requester> findAllRequestersForApproval();
	@Transactional
	@Modifying
	@Query("update Requester r set r.status = :status where r.requesterId = :requesterId")
	int updateRequesterStatus(@Param("status") String status, @Param("requesterId") Long requesterId);
}
